################################################################################

# Fitness function for simple WLAP
# Location - allocation of warehouses (factory2WH costs + rent + transp. costs)

fitnessFunSimpleWLAP <- function(x, PRICE, PRICE_FWH, RENT, KM, CST, KM_FWH, WH) {
  # At least one selected warehouse
  if (!any(x == 1)) {
    return(-1000000000000)
  }
  
  # Rent costs for every open warehouse
  currentSolutionRent <- x %*% RENT
  
  # Transportation costs from selected warehouses (columns) to customers (rows)
  # costSel <- cost[, colnames(cost) %in% WH$warehouse[x == 1]]
  kmSel <- KM[, colnames(KM) %in% WH$warehouse[x == 1]]
  
  # In case only one WH selected, need to transfrom to data.frame
  kmSel <- as.data.frame(kmSel)
  colnames(kmSel) <- WH$warehouse[x == 1]
  
  # When no warehouses selected return extremly high cost
  if (!length(dim(kmSel))) {
    return(-1000000000000)
  }
  
  # Find closest warehouse for each customer
  findMin <- apply(kmSel, 1, FUN = min)
  
  # Current solution total distance WH to customes
  currentSolutionTransp <- findMin %*% CST$demand
  
  # Factory to warehouse costs
  findMinIndex <- apply(kmSel, 1, FUN = which.min)
  
  # Amount of epal from factory to each warehouse
  f2w <- c()
  for (i in 1:dim(kmSel)[2]) {
    f2w[[i]] <- sum(CST$demand[which(findMinIndex == i)])
  }
  names(f2w) <- colnames(kmSel)
  
  # Current solution total distance factory to WH
  currentSolutionF2W <- f2w %*% KM_FWH[which(WH$warehouse %in% colnames(kmSel)), 'from_factory']
  
  # Fitness function value for current solution
  currentSolutionFitness <- currentSolutionRent + currentSolutionTransp*PRICE + currentSolutionF2W*PRICE_FWH
  
  return(-currentSolutionFitness)
}
