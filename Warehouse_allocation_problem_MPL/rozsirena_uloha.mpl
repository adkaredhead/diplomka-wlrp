TITLE Warehouse_Location_Problem;

OPTIONS
ExcelWorkBook="optim_new.xlsx";

INDEX
i:=EXCELRANGE("warehouse");
j:=EXCELRANGE("customer");

DATA
km[i,j]:=EXCELRANGE("distance_km");
b[j]:=EXCELRANGE("demand_epal");
f[i]:=EXCELRANGE("rent");
kmf[i]:=EXCELRANGE("factory2WH_km");
c:=EXCELRANGE("price_km");
p:=EXCELRANGE("price_km_truck");
M=1000000;

INTEGER VARIABLES
y[i,j] EXPORT TO EXCELRANGE("quantity");

BINARY VARIABLES
x[i] EXPORT TO EXCELRANGE("allocation");
t[i];

MODEL
MIN total EXPORT TO EXCELRANGE("total") = sum(i,j:c*km*y) + sum(i:f*x) + sum(i:t);

SUBJECT TO
requirement[j]:       sum(i:y[i,j]) = b[j];
inuse[i,j]:                 y[i,j] <= M*x[i];
thendo1[i]: p*kmf[i]*sum(j:y[i,j]) <= t[i] + M*(1-x[i]);
thendo2[i]: p*kmf[i]*sum(j:y[i,j]) >= t[i] - M*(1-x[i]);
A:                     sum(i:x[i]) >= 2;

END