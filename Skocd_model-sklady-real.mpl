TITLE sklady

OPTION
excelworkbook="Data_sklady-real.xlsx";

INDEX
i=excelrange("city");
j=excelrange("city");

DATA
a[j]:=excelrange("Pozadavky_total!aa");
avs:=excelrange("Auto!avs"); 
cost[i]:=excelrange("Pozadavky_total!cost");
d[i,j]:=excelrange("Matice_km!d");
limit[j]:=excelrange("Matice_time_min!limit");
minstock:=excelrange("Sklady!minstock");
p[j]:=excelrange("Pozadavky_total!p");
PC[i]:=excelrange("Pozadavky_total!PC");
pricekm:=excelrange("Auto!pricekm");
t[i,j]:=60*d[i,j]/avs;{excelrange("Matice_time_min!t");}
w:=excelrange("Sklady!w");
y[i]:=EXCELRANGE("Vysledky!y")
BINARY VARIABLES
x[i,j] EXPORT REFILL EXCELSPARSE ("Vysledky!xx");
!y[i] EXPORT TO EXCELRANGE("Vysledky!y");

DECISION VARIABLES
transport[i] EXPORT TO EXCELRANGE("Vysledky!transport");
storage[i] EXPORT TO EXCELRANGE("Vysledky!storage");
personal[i] EXPORT TO EXCELRANGE("Vysledky!personal");
z

MODEL
MIN z EXPORT TO EXCELRANGE("Vysledky!UF")=SUM(i,j:2*pricekm*d*x*p)+SUM(i,j:cost*a*x)+SUM(i,j:PC*a/w*x);

SUBJECT TO
P1[j]:SUM(i:x)=1;
P2[i]:SUM(j:x)<=COUNT(i)*y;
P3[i,j]:t*x<=limit*y;
P4[i]:SUM(j:a[j]*x[i,j])>=minstock*y[i];
TRANS[i]:transport[i]=SUM(j:pricekm*d[i,j]*x[i,j]*p[j]);
STOR[i]:storage[i]=SUM(j:cost[i]*a[j]*x[i,j]);
PERS[i]:personal[i]=SUM(j:PC[i]*a[j]/w*x[i,j]);
END
