### Genetic algorithm 
# Location - allocation of warehouses (factory2DC costs + rent + transp. costs)

# Fitness function
fitnessFun <- function(x) {
  # At least one selected warehouse
  if (!any(x == 1)) {
    return(-1000000000000)
  }
  
  ##### Rent costs for every open warehouse
  currentSolutionRent <- x %*% warehouses$rent
  
  ##### Transportation costs from selected warehouses to customers
  costSel <- cost[, colnames(cost) %in% warehouses$warehouses[x == 1]]
  
  # Dunno why crashes on this !!!!!!!!!!!
  if (!length(dim(costSel))) {
    return(-1000000000000)
  }
  
  findMin <- apply(costSel, 1, FUN = min)
  
  currentSolutionTransp <- findMin %*% customersFinal$demand
  
  ##### Factory to warehouse costs
  findMinIndex <- apply(costSel, 1, FUN = which.min)
  
  # amount of kg from factory to each warehouse
  f2w_kg <- c()
  for (i in 1:dim(costSel)[2]) {
    f2w_kg[[i]]<-sum(customersFinal$demand[which(findMinIndex == i)])
  }
  names(f2w_kg) <- colnames(costSel)
  
  currentSolutionF2W <- f2w_kg %*% warehouses[warehouses$warehouses %in% colnames(costSel), 'f2w_cost']
  
  ###### Fitness function value for current solution
  currentSolutionFitness <- currentSolutionRent + currentSolutionTransp + currentSolutionF2W
  
  return(-currentSolutionFitness)
}

# GA alg
GALocationAllocation2 <- function(iter = 10) {
  # Aux table
  statsGA           <- data.frame(matrix(ncol = 2 + dim(warehouses)[1], 
                                         nrow = 0))
  colnames(statsGA) <- c('time', 'totalCost', warehouses$warehouses)
  
  for (i in 1:iter) {
    SysTime <- Sys.time()
    ga.result <- ga(type    = 'binary',
                    fitness = fitnessFun,
                    nBits   = dim(warehouses)[1],
                    maxiter = 200,
                    monitor = FALSE)
    
    # Solution
    statsGA[i, 'time']                     <- Sys.time() - SysTime
    statsGA[i, 'totalCost']                <- -ga.result@fitnessValue 
    statsGA[i, 3:(2 + dim(warehouses)[1])] <- ga.result@solution
  }
  return(statsGA)
}

