"""Vehicles Routing Problem (VRP)."""
# VRP based on OR Tools by Google
# Seems like the alg uses all the vehicles even if not needed

# Import packages
from __future__ import print_function
from ortools.constraint_solver import routing_enums_pb2
from ortools.constraint_solver import pywrapcp


# Total distance
def get_total_distance(data, routing, solution):
    """Calculate total distance for VRP"""
    total_distance = 0
    for vehicle_id in range(data['num_vehicles']):
        index = routing.Start(vehicle_id)
        route_distance = 0
        while not routing.IsEnd(index):
            previous_index = index
            index = solution.Value(routing.NextVar(index))
            route_distance += routing.GetArcCostForVehicle(previous_index, index, vehicle_id)
        total_distance += route_distance
    return total_distance


def solve_cvrp(km, demand, num_vehicles, depot, veh_cap, time_limit):
    """Solve the CVRP problem."""

    # Create data
    km = km.values.tolist()
    data = {}
    data['distance_matrix'] = km
    data['num_vehicles'] = int(num_vehicles)
    data['depot'] = int(depot)  # Index of depot/warehouse -- Stare Mesto WH
    data['demand'] = demand['demand']
    data['vehicle_capacities'] = veh_cap  # max(demand)=1645 -- sum(demand)=6110

    # Create the routing index manager
    # Inputs for RoutingIndexManager
    # The number of rows of the distance matrix, which is the number of locations (including the depot)
    # The number of vehicles in the problem
    # The node corresponding to the depot
    manager = pywrapcp.RoutingIndexManager(len(data['distance_matrix']),
                                           data['num_vehicles'],
                                           data['depot'])

    # Create Routing Model
    routing = pywrapcp.RoutingModel(manager)

    # Create and register a transit callback
    # function that takes any pair of locations and returns the distance between them
    def distance_callback(from_index, to_index):
        """Returns the distance between the two nodes."""
        # Convert from routing variable Index to distance matrix NodeIndex
        from_node = manager.IndexToNode(from_index)
        to_node = manager.IndexToNode(to_index)
        return data['distance_matrix'][from_node][to_node]

    transit_callback_index = routing.RegisterTransitCallback(distance_callback)

    # Define cost of each arc -- using directly cost matrix (not distance)
    routing.SetArcCostEvaluatorOfAllVehicles(transit_callback_index)

    # Create demand callback -- bcs of the vehicle capacities
    def demand_callback(from_index):
        """Returns the demand of the node."""
        # Convert from routing variable Index to demands NodeIndex.
        from_node = manager.IndexToNode(from_index)
        return data['demand'][from_node]

    demand_callback_index = routing.RegisterUnaryTransitCallback(
        demand_callback)

    # Can use also AddDimension method in case of same capacity for all vehicles
    routing.AddDimensionWithVehicleCapacity(
        demand_callback_index,       # callback index
        0,                           # null capacity slack
        data['vehicle_capacities'],  # vehicle maximum capacities
        True,                        # start cumul to zero
        'Capacity')

    # Add Distance constraint
    dimension_name = 'Distance'
    routing.AddDimension(
        transit_callback_index,  # callback index
        0,                       # no slack
        1000000,                 # vehicle max travel distance -- probs need a big number
        True,                    # start cumul to zero
        dimension_name)
    distance_dimension = routing.GetDimensionOrDie(dimension_name)
    distance_dimension.SetGlobalSpanCostCoefficient(1000000)

    # Setting first solution heuristic
    # PATH_CHEAPEST_ARC creates an initial route for the solver by repeatedly adding edges with the least weight
    # that don't lead to a previously visited node (other than the depot)
    search_parameters = pywrapcp.DefaultRoutingSearchParameters()
    search_parameters.first_solution_strategy = (
        routing_enums_pb2.FirstSolutionStrategy.PATH_CHEAPEST_ARC)

    # Define the search metaheuristic
    # Automatic
    search_parameters = pywrapcp.DefaultRoutingSearchParameters()
    # or GREEDY_DESCENT, GUIDED_LOCAL_SEARCH,SIMULATED_ANNEALING, TABU_SEARCH -- uncomment following + edit
    # search_parameters.local_search_metaheuristic = (
    #     routing_enums_pb2.LocalSearchMetaheuristic.GUIDED_LOCAL_SEARCH)
    # Set time limit for solving time
    search_parameters.time_limit.seconds = int(time_limit)
    # Logging isn't required but might be useful for debugging
    # search_parameters.log_search = True

    # Solve the problem
    solution = routing.SolveWithParameters(search_parameters)

    # Get total route distance
    total_distance = get_total_distance(data, routing, solution)

    return total_distance
