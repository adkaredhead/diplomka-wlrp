################################################################################
# Genetic algorithm for warehouse location allocation problem +VRP
# Distance to customers computed via Nearest Neighbour alg for VRP

# Optim: ??
# Total Costs: ??
# InitPop w/ 50 individuals each of 1 random WH

# Costs from factory to WH included (fitnessFun2Const / fitnessFun2)

# No results yet.. I ran the code over nigh without any results
# For now trying to work on something else, later might come back to this to
# optimize the speeds
################################################################################

### Clear environment
rm(list = ls())

### Dependencies
library(dplyr)
library(tidyverse)
library(GA)
library(doParallel)
library(parallel)
library(Matrix)
library(reticulate)

### Source function - Python 
use_condaenv('diplomka')

### Load data
# All customers considered as a warehouse
wh      <- read.csv2('WLAP_VRP_R_Python/all_cust_as_wh/warehouses_all_cust.csv')
# Original demand [epal]
# cst     <- read.csv2('WLAP_VRP_R_Python/all_cust_as_wh/customers_all.csv')
# Adjusted demand so that it's from (0,1000>
cst     <- read.csv2('WLAP_VRP_R_Python/all_cust_as_wh/customers_all_adj_demand.csv')
km      <- read.csv2('WLAP_VRP_R_Python/all_cust_as_wh/distance_matrix_km_full_cust.csv')
km_fwh  <- read.csv2('WLAP_VRP_R_Python/all_cust_as_wh/distance_matrix_factory_WH_aka_cust.csv')


# # Original
# price_km     <- 19.4 / 15
# price_km_fwh <- 24 / 34
# rent         <- rep.int(1000000, times = dim(wh)[1])

### Notes
# customer demand in epal
# costs from WH to customer: km * 19.4 / 15
# costs from factory to WH:  km * 24 / 34


# Adjusted costs
price_km     <- 5
price_km_fwh <- 2
rent         <- rep.int(1000000, times = dim(wh)[1])


### Data preparation
rownames(km) <- km$X
km           <- km[, 2:ncol(km)]
colnames(km) <- rownames(km)

colnames(km_fwh)[1] <- 'warehouse'

wh$warehouse     <- as.character(wh$warehouse)
cst$customer     <- as.character(cst$customer)
km_fwh$warehouse <- as.character(km_fwh$warehouse)

# Force symmetric distance matrix
# Needed for OR Tools Python alg
km <- forceSymmetric(as.matrix(km))

# Make a data.frame --- needed??
km <- as.data.frame(as.matrix(km))


################################################################################
### Genetic algorithm 

## Fitness function w/ transp. costs to the warehouses
# Location - allocation of warehouses (factory2DC costs + rent + transp. costs)
fitnessFun <- function(x) {
  
  # At least one selected warehouse
  if (!any(x == 1)) {
    return(-1000000000000)
  }
  
  # Rent costs for every open warehouse
  currentSolutionRent <- x %*% rent
  
  # Transportation costs from selected warehouses (columns) to customers (rows)
  # costSel <- cost[, colnames(cost) %in% wh$warehouse[x == 1]]
  kmSel <- km[, colnames(km) %in% wh$warehouse[x == 1]]
  
  # In case only one WH selected, need to transfrom to data.frame
  kmSel           <- as.data.frame(kmSel)
  colnames(kmSel) <- wh$warehouse[x == 1]
  rownames(kmSel) <- wh$warehouse
  
  # When no warehouses selected return extremly high cost
  if (!length(dim(kmSel))) {
    return(-1000000000000)
  }
  
  # Factory to warehouse costs
  findMinIndex <- apply(kmSel, 1, FUN = which.min)
  
  # Amount of epal from factory to each warehouse
  f2w <- c()
  for (i in 1:dim(kmSel)[2]) {
    f2w[[i]] <- sum(cst$demand[which(findMinIndex == i)])
  }
  names(f2w) <- colnames(kmSel)
  
  # Current solution total distance factory to WH
  currentSolutionF2W <- f2w %*% km_fwh[which(wh$warehouse %in% colnames(kmSel)), 'from_factory']
  
  ##############################################################################
  ### VRP
  
  total_route <- 0
  
  # Create separate data for each WH
  for (i in 1:length(wh$warehouse[x == 1])) {
    
    if (!is.data.frame(km[rownames(km) %in% names(findMinIndex[findMinIndex == i]),
                          colnames(km) %in% names(findMinIndex[findMinIndex == i])])) {
      
      # If WH is distributing only to the town it's located in
      # e.i. the condition above is not a data.frame but a single integer
      
      best_route <- 0
      
    } else {
      # All other cases (WH delivering to multiple towns)
      
      # Distance matrixes (incl. WH)
      assign(paste0('dt', i),
             rbind(`WH` = c(0, kmSel[rownames(kmSel) %in% names(findMinIndex[findMinIndex == i]),
                                     colnames(kmSel) %in% wh$warehouse[x == 1][i]]),
                   cbind(`WH` = kmSel[rownames(kmSel) %in% names(findMinIndex[findMinIndex == i]),
                                      colnames(kmSel) %in% wh$warehouse[x == 1][i]],
                         km[rownames(km) %in% names(findMinIndex[findMinIndex == i]),
                            colnames(km) %in% names(findMinIndex[findMinIndex == i])])))
      
      # Demand lists (incl. WH)
      assign(paste0('cst', i),
             data.frame(customer = as.character(
               c('WH',
                 cst[cst$customer %in%  names(findMinIndex[findMinIndex == i]), 1])),
               demand = as.numeric(
                 c(0,
                   cst[cst$customer %in%  names(findMinIndex[findMinIndex == i]), 2]))))
      
      
      # Need to source the Python script inside fitness function to run in parallel
      source_python('WLAP_VRP_R_Python/VRP_fitness_Python.py')
      
      # Run the selected algorithm in Python to solve CVRP for each WH
      best_route <- solve_cvrp(km           = get(paste0('dt', i)),
                               demand       = get(paste0('cst', i)),
                               num_vehicles = 18,
                               depot        = 0,
                               veh_cap      = rep.int(1000, 18),
                               time_limit   = 30)
    }
    
    # Save the total length of routes across all WHs
    total_route <- total_route + best_route
    
  }
  
  # Transportation costs (VRP)
  currentSolutionTransp <- total_route
  
  ##############################################################################
  
  # Fitness function value for current solution
  currentSolutionFitness <- currentSolutionRent + currentSolutionTransp*price_km + currentSolutionF2W*price_km_fwh
  
  return(-currentSolutionFitness)
}



# ## Initial population of 50 -- each has only one wh
# rnd_nr  <- runif(50, min = 0, max = (length(wh$warehouse)-1))
# rnd_nr  <- sapply(rnd_nr, ceiling)
# initPop <- matrix(0, nrow = 50, ncol = length(wh$warehouse))
# for (i in 1:dim(initPop)[1]) {
#   initPop[i, rnd_nr[i]] <- 1
# }

## Initial population of 50 -- each has only one to five wh
nr_wh_rnd <- runif(n = 50, min = 0, max = 5)
nr_wh_rnd <- sapply(nr_wh_rnd, ceiling)
rnd_pop   <- data.frame(matrix(nrow = 50, ncol = 5))
for (i in 1:50)  {
  rnd_pop[i, 1:nr_wh_rnd[i]] <- sapply(runif(n   = nr_wh_rnd[i], 
                                             min = 0, 
                                             max = length(wh$warehouse)), 
                                       ceiling)
}
initPop <- matrix(0, nrow = 50, ncol = length(wh$warehouse))
for (i in 1:dim(initPop)[1]) {
  selWh <- as.numeric(rnd_pop[i, ])
  selWh <- selWh[!is.na(selWh)]
  initPop[i, selWh] <- 1
}

## GA alg
GALocationAllocation <- function(iter = 20) {
  # Aux table
  statsGA           <- data.frame(matrix(ncol = 2 + dim(wh)[1], 
                                         nrow = 0))
  colnames(statsGA) <- c('time', 'totalCost', wh$warehouse)

  for (i in 1:iter) {
    SysTime   <- Sys.time()
    ga.result <- ga(type        = 'binary',
                    fitness     = fitnessFun,
                    nBits       = dim(wh)[1],
                    suggestions = initPop,
                    maxiter     = 200,
                    monitor     = TRUE,
                    parallel    = 7
                    )
    
    # Solution
    statsGA[i, 'time']             <- Sys.time() - SysTime
    statsGA[i, 'totalCost']        <- -ga.result@fitnessValue 
    statsGA[i, 3:(2 + dim(wh)[1])] <- ga.result@solution
    
    # Print mean and best fitness values of each generation to console
    gaMonitor(ga.result)
  }
  
  return(statsGA)
}


### Run GA
Sys.time()
GA_out <- GALocationAllocation(iter = 1)


################################################################################
# GA output analysis

### Stats
GA_out[, 'nr_wh'] <- apply(GA_out[, 3:ncol(GA_out)], 1, sum)
GA_out[, c(1, 2, ncol(GA_out))]
GA_out[which(GA_out$totalCost == min(GA_out$totalCost)), c(1, 2, ncol(GA_out))]
GA_out[order(GA_out$totalCost), c(1, 2, ncol(GA_out))]

# aa         <- apply(GA_out[, 3:(ncol(GA_out)-1)], 2, sum)
# names(aa)  <- wh$warehouse
# (bb        <- aa[which(aa >= 1)])
# 
# (cc <- GA_out[order(GA_out$totalCost), colnames(GA_out) %in% c('time', 'totalCost', 'nr_wh', names(bb))])

# aa1         <- apply(GA_out[, 3:(ncol(GA_out)-1)], 2, sum)
# names(aa1)  <- wh$warehouse
# (bb1        <- aa1[which(aa1 >= 1)])
# 
# (cc1 <- GA_out[order(GA_out$totalCost), colnames(GA_out) %in% c('time', 'totalCost', 'nr_wh', names(bb1))])

aa2         <- apply(GA_out[, 3:(ncol(GA_out)-1)], 2, sum)
names(aa2)  <- wh$warehouse
(bb2        <- aa2[which(aa2 >= 1)])

cc2 <- GA_out[order(GA_out$totalCost), colnames(GA_out) %in% c('time', 'totalCost', 'nr_wh', names(bb2))]
cc2[1, ]

##### Now with random initPop of 25 w/ only 1 WH

### Save results
# write.csv2(GA_out, 'WLAP_VRP_R/all_cust_as_wh/results_GA_adjDemand_init50_rnd1-5_1iter_200gen_100NN.csv')


