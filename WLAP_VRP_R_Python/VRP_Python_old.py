"""Vehicles Routing Problem (VRP)."""
# VRP based on OR Tools by Google

# for Stare Mesto (4 vehicles each 2000 capacity):
# Total distance of all routes: 2543 price/km
# Total load of all routes: 6110
# Time elapsed: 130.20s

# for Stare Mesto (4 vehicles each 2000 capacity):
# Total distance of all routes: 3194 price/km
# Total load of all routes: 6110
# Time elapsed: 189.20s

# Seems like the alg uses all the vehicles even if not needed

# Import packages
from __future__ import print_function
from ortools.constraint_solver import routing_enums_pb2
from ortools.constraint_solver import pywrapcp
import pandas as pd
import time


def create_data_model():
    """Stores the data for the problem."""

    # Read distance matrix
    dt = pd.read_csv('VRP_Python/km_SM_symmetric_separateWH.csv', sep=';', decimal=',')
    dt = dt.set_index('Unnamed: 0')
    dt = dt.values.tolist()

    # Read demand list
    demand = pd.read_csv('VRP_Python/cust_demand_SM_separateWH.csv', sep=';')

    # Create data
    data = {}
    data['distance_matrix'] = dt
    data['num_vehicles'] = 7
    data['depot'] = 0  # Index of depot/warehouse -- Stare Mesto WH
    data['demand'] = demand['demand']
    data['vehicle_capacities'] = [1000]*7  # max(demand)=1645 -- sum(demand)=6110
    return data


def print_solution(data, manager, routing, solution):
    """Prints solution on console."""
    total_distance = 0
    total_load = 0
    for vehicle_id in range(data['num_vehicles']):
        index = routing.Start(vehicle_id)
        plan_output = 'Route for vehicle {}:\n'.format(vehicle_id)
        route_distance = 0
        route_load = 0
        while not routing.IsEnd(index):
            node_index = manager.IndexToNode(index)
            route_load += data['demand'][node_index]
            plan_output += ' {0} Load({1}) -> '.format(node_index, route_load)
            previous_index = index
            index = solution.Value(routing.NextVar(index))
            route_distance += routing.GetArcCostForVehicle(previous_index, index, vehicle_id)
        plan_output += ' {0} Load({1})\n'.format(manager.IndexToNode(index),
                                                 route_load)
        plan_output += 'Distance of the route: {} price/km\n'.format(route_distance)
        plan_output += 'Load of the route: {}\n'.format(route_load)
        print(plan_output)
        total_distance += route_distance
        total_load += route_load
    print('Total distance of all routes: {} price/km'.format(total_distance))
    print('Total load of all routes: {}'.format(total_load))


# Save results
def get_routes(solution, routing, manager):
    """Get vehicle routes from a solution and store them in an array."""
    # Get vehicle routes and store them in a two dimensional array whose
    # i,j entry is the jth location visited by vehicle i along its route.
    routes = []
    for route_nbr in range(routing.vehicles()):
        index = routing.Start(route_nbr)
        route = [manager.IndexToNode(index)]
        while not routing.IsEnd(index):
            index = solution.Value(routing.NextVar(index))
            route.append(manager.IndexToNode(index))
        routes.append(route)
    return routes


def main():
    """Solve the CVRP problem."""
    # Instantiate the data problem
    data = create_data_model()

    # Create the routing index manager
    # Inputs for RoutingIndexManager
    # The number of rows of the distance matrix, which is the number of locations (including the depot)
    # The number of vehicles in the problem
    # The node corresponding to the depot
    manager = pywrapcp.RoutingIndexManager(len(data['distance_matrix']),
                                           data['num_vehicles'],
                                           data['depot'])

    # Create Routing Model
    routing = pywrapcp.RoutingModel(manager)

    # Create and register a transit callback
    # function that takes any pair of locations and returns the distance between them

    # Probs some mistake here !!!
    def distance_callback(from_index, to_index):
        """Returns the distance between the two nodes."""
        # Convert from routing variable Index to distance matrix NodeIndex
        from_node = manager.IndexToNode(from_index)
        to_node = manager.IndexToNode(to_index)
        return data['distance_matrix'][from_node][to_node]

    transit_callback_index = routing.RegisterTransitCallback(distance_callback)

    # Define cost of each arc -- using directly cost matrix (not distance)
    routing.SetArcCostEvaluatorOfAllVehicles(transit_callback_index)

    # Create demand callback -- bcs of the vehicle capacities
    def demand_callback(from_index):
        """Returns the demand of the node."""
        # Convert from routing variable Index to demands NodeIndex.
        from_node = manager.IndexToNode(from_index)
        return data['demand'][from_node]

    demand_callback_index = routing.RegisterUnaryTransitCallback(
        demand_callback)
    # Can use also AddDimension method in case of same capacity for all vehicles
    routing.AddDimensionWithVehicleCapacity(
        demand_callback_index,       # callback index
        0,                           # null capacity slack
        data['vehicle_capacities'],  # vehicle maximum capacities
        True,                        # start cumul to zero
        'Capacity')

    # Add Distance constraint
    dimension_name = 'Distance'
    routing.AddDimension(
        transit_callback_index,  # callback index
        0,                       # no slack
        1000000,                 # vehicle max travel distance -- probs need a big number
        True,                    # start cumul to zero
        dimension_name)
    distance_dimension = routing.GetDimensionOrDie(dimension_name)
    distance_dimension.SetGlobalSpanCostCoefficient(1000000)

    # Setting first solution heuristic
    # PATH_CHEAPEST_ARC creates an initial route for the solver by repeatedly adding edges with the least weight
    # that don't lead to a previously visited node (other than the depot)
    search_parameters = pywrapcp.DefaultRoutingSearchParameters()
    search_parameters.first_solution_strategy = (
        routing_enums_pb2.FirstSolutionStrategy.PATH_CHEAPEST_ARC)

    # Solve the problem
    solution = routing.SolveWithParameters(search_parameters)

    # Print solution on console
    if solution:
        print_solution(data, manager, routing, solution)

    # # Display the routes -- from save
    # routes = get_routes(solution, routing, manager)
    # for i, route in enumerate(routes):
    #     print('Route', i, route)


start_time = time.time()
if __name__ == '__main__':
    main()

# Print run time
print("Time elapsed: {:.2f}s".format(time.time() - start_time))
